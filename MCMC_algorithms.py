# ========================================================================
# Created by:
# Felipe Uribe @ DTU compute
# ========================================================================
# Version 2020-06
# ========================================================================
import time
import scipy as sp
import scipy.stats as sps
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import progressbar as progress
eps = np.finfo(float).eps

def init_list_of_objects(size):
    list_of_objects = list()
    for i in range(0,size):
        list_of_objects.append( list() ) #different object reference each time
    return list_of_objects



#=========================================================================
#=========================================================================
#=========================================================================
def HMC(Ns, pi_target, grad_pi, x0, epsilonf, L):
    d = len(x0)
    
    # allocation
    samples = np.empty((d, Ns))
    target_eval = np.empty(Ns)
    acc = np.zeros(Ns)

    # initial state    
    samples[:,0] = x0
    target_eval[0] = pi_target(x0)
    acc[0] = 1

    # run MCMC
    for s in range(Ns-1):
        epsilon = epsilonf(1)
        
        # run for each sample all components
        samples[:,s+1], target_eval[s+1], acc[s+1] = HMC_single(d, pi_target, grad_pi, epsilon, L, target_eval[s], samples[:,s])
        
        if (s % 1e2) == 0:
            print('Sample', s, '/', Ns)
    
    print('\nrejection rate:', 1-acc.mean(), '\n')
                  
    return samples, target_eval, acc

#=========================================================================
def HMC_single(d, U, grad_U, epsilon, L, U_old, q_old):
    q = np.copy(q_old)         # initial position (parameters)
    p = np.random.randn(d)     # initial momentum vector
    p_old = np.copy(p)
    
    # LEAPFROG: alternate full steps for position and momentum
    # ticks = time.time()
    q_traj, p_traj = leapfrog(q_old, p_old, L, epsilon, grad_U)
    q, p = q_traj[:,-1], -p_traj[:,-1]
    # tf2 = time.time() - ticks
    # plt.figure()
    # plt.plot(q_traj[0,:],q_traj[1,:],'b.--')
    # plt.axis('equal')
    # plt.figure()
    # plt.plot(p_traj[0,:],p_traj[1,:],'b.-')
    # plt.axis('equal')
    # plt.show()
    
    # evaluate potential and kinetic energies at start and end of trajectory
    # U_old,  K_old  = U(q_old), 0.5*sum(p_old**2)
    K_old = 0.5*sum(p_old**2)
    U_star, K_star = U(q), 0.5*sum(p**2)
    
    # accept/reject
    alpha = np.exp(U_old-U_star + K_old-K_star)    
    uu = np.random.rand()
    if (uu <= alpha):
        acc = 1
        q_new = q
        U_eval = U_star
    else:
        acc = 0
        q_new = q_old
        U_eval = U_old
    
    return q_new, U_eval, acc
#=========================================================================
def leapfrog(q0, p0, L, epsilon, grad_U):
    # symplectic integrator: trajectories preserve phase space volumen
    # L = np.floor(T/epsilon)
    q, p = np.empty((len(q0),L+1)), np.empty((len(q0),L+1))
    q[:,0], p[:,0] = q0, p0
    for n in range(L):
        p_n_plus_half = p[:,n] - (epsilon/2)*grad_U(q[:,n])
        q[:,n+1] = q[:,n] + (epsilon)*p_n_plus_half
        p[:,n+1] = p_n_plus_half - (epsilon/2)*grad_U(q[:,n+1])
    
    # faster: do not store trajectory
    # q, p = np.copy(q0), np.copy(p0)
    # p -= (epsilon/2)*grad_U(q)   # initial half step for momentum 
    # for n in range(L):
    #     q += epsilon*p   # full step for the position
    #     if (n != L-1):
    #         p -= epsilon*grad_U(q)   # full step for the momentum, skip last one
    # p -= (epsilon/2)*grad_U(q)   # final half step for momentum 
    # p = -p                       # negate to make proposal symmetric
        
    return q, p


#=========================================================================
#=========================================================================
#=========================================================================
def RWM(Ns, Nb, pi_target, x0, betaf):
    d = len(x0)
    
    # allocation
    samples = np.empty((d, Ns))
    target_eval = np.empty(Ns)
    acc = np.zeros(Ns)

    # initial state    
    samples[:,0] = x0
    target_eval[0] = pi_target(x0)
    acc[0] = 1

    # run MCMC
    #for s in range(Ns-1):
    for s in progress.progressbar( range(Ns-1) ):
        beta = betaf(1)
        
        # run for each sample all components
        samples[:,s+1], target_eval[s+1], acc[s+1] = RWM_single(d, Nb, pi_target, beta, target_eval[s], samples[:,s])
        
    print('\nrejection rate:', 1-acc.mean(), '\n')
    
    return samples, target_eval, acc

#=========================================================================
def RWM_single(d, Nb, pi_target, beta, target_eval_old, x_old): 
    x_new = np.copy(x_old)
    target_eval_new = np.copy(target_eval_old)
    # traject = list()
    it = 0
    while True:   
        it += 1
        # traject.append(np.copy(x_new))
        x_star = x_new + beta*np.random.randn(d)    # propose state         
        
        # evaluate target
        target_eval_star = pi_target(x_star)
    
        # ratio and acceptance probability
        ratio = np.exp(target_eval_star - target_eval_new)  # proposal is symmetric
        alpha = min(1, ratio)
    
        # accept/reject
        uu = np.random.rand()
        if (uu <= alpha):
            acc = 1
            x_new = x_star
            target_eval_new = target_eval_star
        else:
            acc = 0
            #x_new = x_new
            #target_eval_new = target_eval_new
        if (it == Nb):
            break
    # qq = np.array(traject)
    # plt.figure(1)
    # plt.plot(qq[:,0],qq[:,1],'.:')
    # plt.axis('equal')
    # plt.xlim([-2,2])
    # plt.ylim([-2,2])
    # plt.show()
            
    return x_new, target_eval_new, acc



#=========================================================================
#=========================================================================
#=========================================================================
def pCN(Ns, Nb, pi_target, x0, betaf, T):
    d = len(x0)
    
    # allocation
    samples = np.empty((d, Ns))
    target_eval = np.empty(Ns)
    acc = np.zeros(Ns)

    # initial state    
    samples[:,0] = x0
    target_eval[0] = pi_target(x0)
    acc[0] = 1

    # run MCMC
    for s in range(Ns-1):
        beta = betaf(1)
        
        # run for each sample all components
        samples[:,s+1], target_eval[s+1], acc[s+1] = pCN_single(d, Nb, pi_target, beta, T, target_eval[s], samples[:,s])
        
        if (s % 1e2) == 0:
            print('Sample', s, '/', Ns)
    
    print('\nrejection rate:', 1-acc.mean(), '\n')
    
    return samples, target_eval, acc

#=========================================================================
def pCN_single(d, Nb, pi_target, beta, T, target_eval_old, x_old): 
    d = len(x_old)
    x_new = np.copy(x_old)
    target_eval_new = np.copy(target_eval_old)
    # traject = list()
    it = 0
    while True:   
        it += 1
        xi = np.random.randn(d)#np.random.normal(np.zeros(d), Sigma_pr)
        x_star = np.sqrt(1-beta**2)*x_new + beta*xi   # propose state         
        
        # evaluate target
        target_eval_star = pi_target(T(x_star))
    
        # ratio and acceptance probability
        ratio = np.exp(target_eval_star - target_eval_new)  
        alpha = min(1, ratio)
    
        # accept/reject
        uu = np.random.rand()
        if (uu <= alpha):
            acc = 1
            x_new = x_star
            target_eval_new = target_eval_star
        else:
            acc = 0
            #x_new = x_new
            #target_eval_new = target_eval_new
        if (it == Nb):
            break
    # qq = np.array(traject)
    # plt.figure(1)
    # plt.plot(qq[:,0],qq[:,1],'.:')
    # plt.axis('equal')
    # plt.xlim([-2,2])
    # plt.ylim([-2,2])
    # plt.show()
            
    return x_new, target_eval_new, acc



#=========================================================================
#=========================================================================
#=========================================================================
def CWMH(Ns, pi_target, proposal, x0, scale):
    n = len(x0)
    
    # allocation
    samples = np.empty((n, Ns))
    target_eval = np.empty(Ns)
    acc = np.zeros((n, Ns))

    # initial state    
    samples[:,0] = x0
    target_eval[0] = pi_target(x0)
    acc[:,0] = np.ones(n)

    # run MCMC
    for s in range(Ns-1):
        # run for each sample component by component
        samples[:,s+1], target_eval[s+1], acc[:,s+1] = CWMH_single(n, samples[:,s], target_eval[s], pi_target, proposal, scale)
        
        if (s % 5e2) == 0:
            print('Sample', s, '/', Ns)
                  
    return samples, target_eval, acc

#=========================================================================
def CWMH_single(n, x_t, target_eval_t, pi_target, proposal, scale):
    #
    x_i_star = proposal(1, x_t, scale)
    x_star = np.copy(x_t)
    acc = np.zeros(n)
    #
    for j in range(n):
        # propose state
        x_star[j] = x_i_star[0,j]
        
        # evaluate target
        target_eval_star = pi_target(x_star)
        
        # ratio and acceptance probability
        ratio = np.exp(target_eval_star - target_eval_t)  # proposal is symmetric
        alpha = min(1, ratio)

        # accept/reject
        u_theta = np.random.rand()
        if (u_theta <= alpha):
            x_t[j] = x_i_star[0,j]
            target_eval_t = target_eval_star
            acc[j] = 1
        else:
            pass
            # x_t[j]       = x_t[j]
            # target_eval_t = target_eval_t
        x_star = np.copy(x_t)
        
    return x_t, target_eval_t, acc
import numpy as np
import matplotlib.pyplot as plt
#from shapely.geometry import Point
#from shapely.geometry.polygon import Polygon

class StarField:
	def __init__(self, N=200, num_terms=20, regul=3):
		self.theta = np.linspace(0,2*np.pi,N)
		self.a = []
		self.b = []
		self.p0 = 0.2
		self.KL_terms = num_terms
		self.regul = regul

		self.x = []

		self.x_ = []

		self.rmat = []


	def realize_field(self):
		check = False
		while( not(check) ):
			self.compute_field()
			check = self.not_outside(self.r)

	def set_params(self,params):
		self.p0 = 0.2
		self.a = params[0:self.KL_terms-1]
		self.b = params[self.KL_terms-1:]
		self.r = []

		for i in range( self.theta.shape[0] ):
			ft = self.p0/np.sqrt( 2*np.pi )
			#ft = 0.
			for j in range(self.KL_terms-1):
				ft += self.a[j]*np.cos((j+1)*self.theta[i])/(j+1)**self.regul/np.sqrt(np.pi) + self.b[j]*np.sin((j+1)*self.theta[i])/(j+1)**self.regul/np.sqrt(np.pi)
			self.r.append( 0.25*np.exp( ft ) )
		self.r = np.array( self.r )

		self.x = []

		for i in range(self.theta.shape[0]):
			self.x.append([ self.r[i]*np.cos(self.theta[i]), self.r[i]*np.sin(self.theta[i])] )

		self.x = np.array(self.x)

	def get_params(self):
		vec = np.zeros(2*self.KL_terms-2)
		vec[0:self.KL_terms-1] = self.a
		vec[self.KL_terms-1:] = self.b
		return vec

	def give_curve_points(self):
		x = self.r*np.cos( self.theta - np.pi/2)
		y = self.r*np.sin( self.theta -np.pi/2)
		return x, y

	def not_outside(self,r):
		check = True
		for i in range(r.shape[0]):
			check = check and r[i] < 1.
		return check

	def compute_field(self):
		self.p0 = 0.2
		self.a = np.random.normal(0,1,self.KL_terms-1)
		self.b = np.random.normal(0,1,self.KL_terms-1)
		self.r = []

		for i in range( self.theta.shape[0] ):
			ft = self.p0/np.sqrt( 2*np.pi )
			#ft = 0.
			for j in range(self.KL_terms-1):
				ft += self.a[j]*np.cos((j+1)*self.theta[i])/(j+1)**self.regul/np.sqrt(np.pi) + self.b[j]*np.sin((j+1)*self.theta[i])/(j+1)**self.regul/np.sqrt(np.pi)
			self.r.append( 0.25*np.exp( ft ) )
		self.r = np.array( self.r )

		self.x = []

		for i in range(self.theta.shape[0]):
			self.x.append([ self.r[i]*np.cos(self.theta[i]), self.r[i]*np.sin(self.theta[i])] )

		self.x = np.array(self.x)


	def generate_rmat(self,t):
		self.rmat = np.reshape( [np.cos(-t), -np.sin(-t), np.sin(-t), np.cos(-t)], [2,2] )

	def rotate(self):
		self.x_ = (self.rmat@self.x.T).T
		#self.polygon = Polygon(self.x_[:-1,:])

	def set_rand_walk_size(self, eps):
		self.eps = eps

	def rand_walk(self):
		check = False
		while( not(check) ):
			p0_new = self.p0 + np.random.uniform(0,self.eps)
			a_new = self.a + np.random.uniform(0,self.eps,self.a.shape)
			b_new = self.b + np.random.uniform(0,self.eps,self.b.shape)

			r = []
			for i in range( self.theta.shape[0] ):
				ft = p0_new/np.sqrt( 2*np.pi )
				#ft = 0.
				for j in range(self.KL_terms-1):
					ft += a_new[j]*np.cos((j+1)*self.theta[i])/(j+1)**2/np.sqrt(np.pi) + b_new[j]*np.sin((j+1)*self.theta[i])/(j+1)**2/np.sqrt(np.pi)
				r.append( 0.25*np.exp( ft ) )
			r = np.array(r)
			check = self.not_outside(r)

		self.p0 = p0_new
		self.a = a_new
		self.b = b_new
		self.r = r

		self.x = []

		for i in range(self.theta.shape[0]):
			self.x.append([ self.r[i]*np.cos(self.theta[i]), self.r[i]*np.sin(self.theta[i])] )

		self.x = np.array(self.x)

# uncomment for polynomial check
#	def contains(self,p):
#		point = Point(p)
#		return self.polygon.contains(point)

# uncomment for radial check
	def contains(self,p,theta):
		r = np.sqrt( p[0]**2 + p[1]**2 )
		t = np.arctan2(p[1],p[0]) - theta
		if t < 0:
			t += 2*np.pi

		#I = np.argwhere(self.theta < t)
		#
		#tau = I[-1]
		tau = int( t*200./2/np.pi )
		return r < self.r[tau]

	def contains_ref(self,p):
		r = np.sqrt( p[0]**2 + p[1]**2 )
		t = np.arctan2(p[1],p[0])
		if t < 0:
			t += 2*np.pi
		tau = int( t*200./2/np.pi )
		return r < self.r[tau]



	def compute_image(self,N=100):
		im = np.zeros([N,N])
		x_coor = np.linspace(-1,1,N)
		y_coor = np.linspace(-1,1,N)
		i=0
		for x in x_coor:
			j=0
			for y in y_coor:
				if x*x + y*y > 1.0:
					im[i,j] = 0.0
				if self.contains_ref(np.array([x,y])):
					im[i,j] = 1.0
				else:
					im[i,j] = 0
				j+=1
			i+=1
		return im

	def plot_field(self):
		self.fig, self.ax = plt.subplots()
		c1 = plt.Circle((0,0),1,color='k',fill=False)
		self.ax.add_artist(c1)

		plt.plot(self.x.T[0],self.x.T[1],'b-')
		self.ax.set_xlim([-1,1])
		self.ax.set_ylim([-1,1])
		self.ax.set_aspect('equal',adjustable='box')
		plt.show()

	def save_field(self,path='field.eps'):
		self.fig, self.ax = plt.subplots()
		c1 = plt.Circle((0,0),1,color='k',fill=False)
		self.ax.add_artist(c1)

		plt.plot(self.x.T[0],self.x.T[1],'b-')
		self.ax.set_xlim([-1,1])
		self.ax.set_ylim([-1,1])
		self.ax.set_aspect('equal',adjustable='box')
		plt.savefig(path, format='eps')

if __name__ == '__main__':
	field = StarField()
	field.realize_field()

	#field.contains(np.array([0.1,0.1]))
	#field.set_rand_walk_size(0.1)
	#field.generate_rmat(np.pi/4.)
	#field.rotate()
	field.plot_field()
	#field.rand_walk()
	#plt.plot( field.field )
	#plt.show()

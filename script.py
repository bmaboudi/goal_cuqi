import numpy as np
import matplotlib.pyplot as plt
from skimage.transform import radon, rescale
from skimage import data
import star_field as star
import time
import MCMC_algorithms as mcmc
import progressbar as progress
import argparse

class p_to_sino():
	def __init__(self,theta0, theta1, n_KL=3, regul=3):
		self.num_rand_terms = 2*(n_KL-1) 
		self.field = star.StarField(N=200, num_terms=n_KL, regul=regul)
		self.field.realize_field()
		im = self.field.compute_image()
		self.theta = np.linspace(theta0, theta1, max(im.shape), endpoint=False)

	def forward(self,p):
		self.field.set_params(p)
		im = self.field.compute_image()
		return radon(im,theta=self.theta, circle=True)

	def give_points(self):
		return self.field.give_curve_points()

	def get_params(self):
		return self.field.get_params()



def save_observation():
	s = star.StarField(N=200, num_terms=3)
	s.realize_field()
	im = s.compute_image()
	p = s.get_params()
	theta = np.linspace(0., 180., max(im.shape), endpoint=False)
	sinogram = radon(im,theta=theta, circle=True)

	np.savez('./observation/obs_data.npz',params=p,image=im,sino=sinogram)

	plt.subplot(121)
	plt.imshow(im)
	plt.colorbar()

	plt.subplot(122)
	plt.imshow(sinogram)
	plt.show()

def save_observation_small_ang(theta0, theta1, obs_path):
	s = star.StarField(N=200, num_terms=3)
	s.realize_field()
	im = s.compute_image()
	p = s.get_params()
	theta = np.linspace(theta0, theta1, max(im.shape), endpoint=False)
	sinogram = radon(im,theta=theta, circle=True)

	np.savez(obs_path,params=p,image=im,sino=sinogram, theta0=theta0, theta1=theta1)

	plt.subplot(121)
	plt.imshow(im)
	plt.colorbar()

	plt.subplot(122)
	plt.imshow(sinogram)
	plt.show()

def save_observation_regularity(theta0, theta1, obs_path, regul=3):
	s = star.StarField(N=200, num_terms=3, regul=regul)
	s.realize_field()
	im = s.compute_image()
	p = s.get_params()
	theta = np.linspace(theta0, theta1, max(im.shape), endpoint=False)
	sinogram = radon(im,theta=theta, circle=True)

	np.savez(obs_path,params=p,image=im,sino=sinogram, theta0=theta0, theta1=theta1)

	plt.subplot(121)
	plt.imshow(im)
	plt.colorbar()

	plt.subplot(122)
	plt.imshow(sinogram)
	plt.show()

def save_observation_kl_reg(theta0, theta1, obs_path, regul=3, kl=3):
	s = star.StarField(N=200, num_terms=kl, regul=regul)
	s.realize_field()
	im = s.compute_image()
	p = s.get_params()
	theta = np.linspace(theta0, theta1, max(im.shape), endpoint=False)
	sinogram = radon(im,theta=theta, circle=True)

	np.savez(obs_path,params=p,image=im,sino=sinogram, theta0=theta0, theta1=theta1, regul=regul, kl_terms=kl)

	plt.subplot(121)
	plt.imshow(im)
	plt.colorbar()

	plt.subplot(122)
	plt.imshow(sinogram)
	plt.show()

def run_mcmc(obs_path, stat_path, regul=3, kl=3):
	input_dim = 4
	obs_data = np.load(obs_path)
	y_obs = obs_data['sino']
	theta0 = obs_data['theta0']
	theta1 = obs_data['theta1']

	forward_map = p_to_sino(theta0,theta1,n_KL=kl, regul=regul)
	forward = lambda p: forward_map.forward(p)

	sigma = 0.005
	log_like = lambda p: - ( 0.5*np.sum(  (forward(p) - y_obs)**2*1e-8)/sigma/sigma + 0.5*np.sum(p**2) )

	Ns = 1000
	Nb = 10
	p0 = forward_map.get_params()

	beta = lambda N: 0.1
	samples, target, _ = mcmc.RWM(Ns, Nb, log_like, p0, beta)
	np.savez(stat_path,samples=samples,target=target)

def compute_var(samples, forward_map, num_element = 0):
	r_data = []
	for i in progress.progressbar( range( samples.shape[1]) ):
	#for i in progress.progressbar( range(100) ):
		temp = np.zeros(num_element.shape[0])
		p = samples[:,i]
		forward_map.forward(p)
		for i in range(len(temp)):
			temp[i] = forward_map.field.r[num_element[i] ]
		r_data.append(temp)
	r_data = np.array(r_data)

	var = np.sum( (r_data - np.mean(r_data,axis=0) )**2 , axis=0)/(r_data.shape[0]-1)
	return var


def post_process(obs_path, stat_path, regul=3, kl=3):
	stats = np.load(stat_path)
	obs_data = np.load(obs_path)
	theta0 = obs_data['theta0']
	theta1 = obs_data['theta1']

	
	forward_map = p_to_sino(theta0,theta1,n_KL=kl ,regul=regul)

	target = stats['target']
	samples = stats['samples']

	mean = np.mean(samples,axis=1)

	num_element= np.array( list(range(200)) )
	var = compute_var(samples, forward_map, num_element=num_element)
	error_len = 3*np.sqrt(var)/2

	temp = forward_map.forward(obs_data['params'])
	points = forward_map.give_points()

	f, ( (ax, ax1), (ax2, ax3) ) = plt.subplots(2,2)
	im = ax.imshow(obs_data['image'])
	ax.set_title('exact image')
	f.colorbar(im,ax=ax)

	ax1.plot(points[0],points[1],'r', label='exact curve')


	mean_out = forward_map.forward(mean)
	points = forward_map.give_points()

	ax1.plot(points[0],points[1],'b', label='predicted curve')
#	for i in range(1,num_element.shape[0]):
#		theta = forward_map.field.theta[num_element[i]] - np.pi/2
#		ex = [ points[0][num_element[i]] - error_len[i]*np.cos(theta) , points[0][num_element[i]] + error_len[i]*np.cos(theta) ]
#		ey = [ points[1][num_element[i]] - error_len[i]*np.sin(theta) , points[1][num_element[i]] + error_len[i]*np.sin(theta) ]
#		ax1.plot( ex,ey,'k')
#	theta = forward_map.field.theta[num_element[0]] - np.pi/2
#	ex = [ points[0][num_element[0]] - error_len[i]*np.cos(theta) , points[0][num_element[0]] + error_len[i]*np.cos(theta) ]
#	ey = [ points[1][num_element[0]] - error_len[i]*np.sin(theta) , points[1][num_element[0]] + error_len[i]*np.sin(theta) ]
#	ax1.plot( np.array(ex),np.array(ey),'k')

	ex_lower = []
	ey_lower = []
	for i in range(0,num_element.shape[0]):
		theta = forward_map.field.theta[num_element[i]] - np.pi/2
		ex_lower.append( points[0][num_element[i]] - error_len[i]*np.cos(theta) )
		ey_lower.append( points[1][num_element[i]] - error_len[i]*np.sin(theta) )
	ex_lower.append( points[0][num_element[0]] - error_len[0]*np.cos(theta) )
	ey_lower.append( points[1][num_element[0]] - error_len[0]*np.sin(theta) )
	ax1.plot(ex_lower,ey_lower,'g')

	ex_upper = []
	ey_upper = []
	for i in range(0,num_element.shape[0]):
		theta = forward_map.field.theta[num_element[i]] - np.pi/2
		ex_upper.append( points[0][num_element[i]] + error_len[i]*np.cos(theta) )
		ey_upper.append( points[1][num_element[i]] + error_len[i]*np.sin(theta) )
	ex_upper.append( points[0][num_element[0]] + error_len[0]*np.cos(theta) )
	ey_upper.append( points[1][num_element[0]] + error_len[0]*np.sin(theta) )
	ax1.plot(ex_upper,ey_upper,'g', label='region of 99\% certainty')

	ax1.set_xlim([-1,1])
	ax1.set_ylim([-1,1])
	ax1.set_aspect('equal',adjustable='box')
	ax1.set_title('solution to the inverse problem with 99\% confidence interval')
	ax1.legend()

	im = ax2.imshow(obs_data['sino'])
	ax2.set_title('exact sinogram')
	f.colorbar(im,ax=ax2)

	im = ax3.imshow(mean_out)
	ax3.set_title('sinogram for the predicted parameter')
	f.colorbar(im,ax=ax3)

	plt.show()

def save_plots(obs_path, stat_path, dest_path, regul=3, kl=3):
	stats = np.load(stat_path)
	obs_data = np.load(obs_path)
	theta0 = obs_data['theta0']
	theta1 = obs_data['theta1']

	
	forward_map = p_to_sino(theta0,theta1,n_KL=kl ,regul=regul)

	target = stats['target']
	samples = stats['samples']

	mean = np.mean(samples,axis=1)

	num_element= np.array( list(range(200)) )
	var = compute_var(samples, forward_map, num_element=num_element)
	error_len = 3*np.sqrt(var)/2

	temp = forward_map.forward(obs_data['params'])
	points = forward_map.give_points()

	fig = plt.figure()
	ax = fig.add_subplot(1,1,1)
	im = ax.imshow(obs_data['image'])
	ax.set_title('exact image')
	ax.set_xlabel('x')
	ax.set_ylabel('y')
	fig.colorbar(im,ax=ax)
	plt.savefig(dest_path+'/true_im.eps')

	fig = plt.figure()
	ax1 = fig.add_subplot(1,1,1)
	ax1.plot(points[0],points[1],'r', label='exact curve')


	mean_out = forward_map.forward(mean)
	points = forward_map.give_points()

	ax1.plot(points[0],points[1],'b', label='predicted curve')
	ex_lower = []
	ey_lower = []
	for i in range(0,num_element.shape[0]):
		theta = forward_map.field.theta[num_element[i]] - np.pi/2
		ex_lower.append( points[0][num_element[i]] - error_len[i]*np.cos(theta) )
		ey_lower.append( points[1][num_element[i]] - error_len[i]*np.sin(theta) )
	ex_lower.append( points[0][num_element[0]] - error_len[0]*np.cos(theta) )
	ey_lower.append( points[1][num_element[0]] - error_len[0]*np.sin(theta) )
	ax1.plot(ex_lower,ey_lower,'g')

	ex_upper = []
	ey_upper = []
	for i in range(0,num_element.shape[0]):
		theta = forward_map.field.theta[num_element[i]] - np.pi/2
		ex_upper.append( points[0][num_element[i]] + error_len[i]*np.cos(theta) )
		ey_upper.append( points[1][num_element[i]] + error_len[i]*np.sin(theta) )
	ex_upper.append( points[0][num_element[0]] + error_len[0]*np.cos(theta) )
	ey_upper.append( points[1][num_element[0]] + error_len[0]*np.sin(theta) )
	ax1.plot(ex_upper,ey_upper,'g', label=r'region of $3\sigma$ certainty')

	ax1.set_xlim([-1,1])
	ax1.set_ylim([-1,1])
	ax1.set_aspect('equal',adjustable='box')
	ax1.legend()
	ax1.set_xlabel('x')
	ax1.set_ylabel('y')
	plt.savefig(dest_path+'/prediction.eps')

	fig = plt.figure()
	ax2 = fig.add_subplot(1,1,1)
	im = ax2.imshow(obs_data['sino'])
	fig.colorbar(im,ax=ax2)
	ax2.set_xlabel('x')
	ax2.set_ylabel('y')
	plt.savefig(dest_path+'/exact_sino.eps')

	fig = plt.figure()
	ax3 = fig.add_subplot(1,1,1)
	im = ax3.imshow(mean_out)
	fig.colorbar(im,ax=ax3)
	ax3.set_xlabel('x')
	ax3.set_ylabel('y')
	plt.savefig(dest_path+'/mean_sino.eps')


	fig = plt.figure()
	ax4 = fig.add_subplot(1,1,1)
	ax4.plot(target)
	ax4.set_xlabel('MCMC sample number')
	ax4.set_ylabel('MCMC log-target value')
	plt.savefig(dest_path+'/target.eps')





def parse_commandline():
	parser = argparse.ArgumentParser(description='Decoder')
	parser.add_argument('-a','--action', help='Action: mcmc, obs_small, obs_reg_2, obs_kl_reg, save_plots', required=True)
	parser.add_argument('-o','--obs-path',help='Observation File Path', required=True)
	parser.add_argument('-s','--stat-path',help='Path For Saving Statistics', required=False)
	parser.add_argument('-r','--regularity',help='Regularity Of The Prior', required=False)
	parser.add_argument('-kl','--num-kl-terms',help='Number Of KL Terms',required=False)
	parser.add_argument('-dp','--dest-path',help='Path To Destination Folder',required=False)

	args = parser.parse_args()
	return args

if __name__ == '__main__':
	args = parse_commandline()

	if(args.regularity):
		regul=float(args.regularity)
	else:
		regul = None

	if(args.num_kl_terms):
		kl=int(args.num_kl_terms)
	else:
		kl = None

	if(args.action == 'obs_small'):
		save_observation_small_ang(0., 30., args.obs_path)
	if(args.action == 'obs_reg_2'):
		save_observation_regularity(0.,180.,args.obs_path,regul=regul)
	if(args.action == 'obs_kl_reg'):
		save_observation_kl_reg(0.,180.,args.obs_path,regul=regul,kl=kl)
	if(args.action == 'mcmc'):
		run_mcmc(args.obs_path, args.stat_path,regul=regul, kl=kl)
	if(args.action == 'post'):
		post_process(args.obs_path, args.stat_path, regul=regul, kl=kl)
	if(args.action == 'save_plots'):
		save_plots(args.obs_path, args.stat_path, args.dest_path, regul=regul, kl=kl)

	#save_observation_small_ang()
	#run_mcmc()
	#post_process()